// Dpsensors provides a unified interface for the Deep Profiler sensors
package dpsensors

import (
	"context"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"

	"bitbucket.org/mfkenney/aanderaa"
	"bitbucket.org/mfkenney/fsi"
	"bitbucket.org/mfkenney/seabird"
	"bitbucket.org/mfkenney/wetlabs"
)

type SerialSensor interface {
	// Send sends a command to the sensor
	Send(cmd string) error
	// Recv returns a response from the sensor
	Recv() (string, error)
	// Exec sends a command to the sensor and returns the response along
	// with any error that occurs.
	Exec(cmd string) (string, error)
	// Start puts the sensor into streaming mode
	Start() error
	// Stop puts the sensor into command mode
	Stop() error
	// Stream returns a channel which will supply raw data records from
	// the sensor. The channel is closed when ctx is cancelled.
	Stream(ctx context.Context) <-chan string
}

type DpSensor interface {
	SerialSensor
	// Return the sensor name
	Name() string
	// ParseData takes a data record string from the sensor
	// and converts it to a map. The map keys are the data
	// variable names.
	ParseData(raw string) (map[string]interface{}, error)
	// Variables returns a list of data variable names
	Variables() []string
}

var ErrRecord = errors.New("Invalid data record")

type Ctd struct {
	*seabird.Device
	name string
	vars []string
}

func NewCtd(name string, rw io.ReadWriter) *Ctd {
	return &Ctd{
		Device: seabird.NewDevice(rw),
		name:   name,
		vars:   []string{"condwat", "tempwat", "preswat"},
	}
}

func (d *Ctd) Name() string {
	return d.name
}

func (d *Ctd) ParseData(raw string) (map[string]interface{}, error) {
	fields := strings.Split(strings.Trim(raw, " S>\t\r\n"), ",")
	if len(fields) < len(d.vars) {
		return nil, ErrRecord
	}

	rec := make(map[string]interface{})
	for i, name := range d.vars {
		x, err := strconv.ParseFloat(strings.Trim(fields[i], " \t"), 32)
		if err != nil {
			return nil, fmt.Errorf("%s: parsing %q: %w", d.name, name, err)
		}
		rec[name] = float32(x)
	}

	return rec, nil
}

func (d *Ctd) Variables() []string {
	return d.vars
}

type Optode struct {
	*aanderaa.Device
	name     string
	vars     []string
	indicies []int
}

type OptodeOption interface {
	apply(*Optode)
}

type funcOptodeOption struct {
	f func(*Optode)
}

func (fo *funcOptodeOption) apply(o *Optode) {
	fo.f(o)
}

func newFuncOptodeOption(f func(*Optode)) *funcOptodeOption {
	return &funcOptodeOption{f: f}
}

func Indicies(is []int) OptodeOption {
	return newFuncOptodeOption(func(o *Optode) {
		o.indicies = is
	})
}

func NewOptode(name string, rw io.ReadWriter, opts ...OptodeOption) *Optode {
	o := &Optode{
		Device:   aanderaa.NewDevice(rw),
		name:     name,
		vars:     []string{"t", "doconcs"},
		indicies: []int{3, 4},
	}
	for _, opt := range opts {
		opt.apply(o)
	}

	return o
}

func (d *Optode) Name() string {
	return d.name
}

func (d *Optode) ParseData(raw string) (map[string]interface{}, error) {
	fields := strings.Split(strings.Trim(raw, " \t\r\n"), "\t")
	rec := make(map[string]interface{})
	n := len(fields)

	for i, j := range d.indicies {
		if j >= n {
			return nil, ErrRecord
		}
		name := d.vars[i]
		x, err := strconv.ParseFloat(strings.Trim(fields[j], " \t"), 32)
		if err != nil {
			return nil, fmt.Errorf("%s: parsing %q: %w", d.name, name, err)
		}
		rec[name] = float32(x)
	}

	return rec, nil
}

func (d *Optode) Variables() []string {
	return d.vars
}

type Fluorometer struct {
	*wetlabs.Device
	name     string
	vars     []string
	indicies []int
}

func (d *Fluorometer) ParseData(raw string) (map[string]interface{}, error) {
	fields := strings.Split(strings.Trim(raw, " \t\r\n"), "\t")
	rec := make(map[string]interface{})
	n := len(fields)

	for i, j := range d.indicies {
		if j >= n {
			return nil, ErrRecord
		}
		name := d.vars[i]
		x, err := strconv.ParseInt(strings.Trim(fields[j], " \t"), 10, 16)
		if err != nil {
			return nil, fmt.Errorf("%s: parsing %q: %w", d.name, name, err)
		}
		rec[name] = int16(x)
	}

	return rec, nil
}

func (d *Fluorometer) Name() string {
	return d.name
}

func (d *Fluorometer) Variables() []string {
	return d.vars
}

type Flntu struct {
	*Fluorometer
}

func NewFlntu(name string, rw io.ReadWriter) *Flntu {
	return &Flntu{&Fluorometer{
		Device:   wetlabs.NewDevice(rw),
		name:     name,
		vars:     []string{"chlaflo", "ntuflo"},
		indicies: []int{3, 5},
	}}
}

type Flcd struct {
	*Fluorometer
}

func NewFlcd(name string, rw io.ReadWriter) *Flcd {
	return &Flcd{&Fluorometer{
		Device:   wetlabs.NewDevice(rw),
		name:     name,
		vars:     []string{"cdomflo"},
		indicies: []int{3},
	}}
}

type Acm struct {
	*fsi.Device
	name     string
	vars     []string
	indicies []int
}

func NewAcm(name string, rw io.ReadWriter) *Acm {
	return &Acm{
		Device: fsi.NewDevice(rw),
		name:   name,
		vars: []string{"tx", "ty",
			"hx", "hy", "hz",
			"va", "vb", "vc", "vd"},
	}
}

func (d *Acm) Name() string {
	return d.name
}

func (d *Acm) Variables() []string {
	return d.vars
}

func (d *Acm) ParseData(raw string) (map[string]interface{}, error) {
	fields := strings.Split(strings.Trim(raw, " \t\r\n"), ",")
	if len(fields) < len(d.vars) {
		return nil, ErrRecord
	}

	rec := make(map[string]interface{})
	for i, name := range d.vars {
		x, err := strconv.ParseFloat(strings.Trim(fields[i], " \t"), 32)
		if err != nil {
			return nil, fmt.Errorf("%s: parsing %q: %w", d.name, name, err)
		}
		rec[name] = float32(x)
	}

	return rec, nil
}
