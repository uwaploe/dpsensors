module bitbucket.org/uwaploe/dpsensors

go 1.16

require (
	bitbucket.org/mfkenney/aanderaa v0.2.1
	bitbucket.org/mfkenney/fsi v0.1.1
	bitbucket.org/mfkenney/seabird v0.2.1
	bitbucket.org/mfkenney/wetlabs v0.2.2
)
