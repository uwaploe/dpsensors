package dpsensors

import (
	"log"
	"reflect"
	"testing"
)

func TestCtd(t *testing.T) {
	table := []struct {
		in  string
		out map[string]interface{}
	}{
		{
			in: "S> 35.4789,  6.9892,  182.25\r\n",
			out: map[string]interface{}{
				"condwat": float32(35.4789),
				"tempwat": float32(6.9892),
				"preswat": float32(182.25)},
		},
	}

	const name = "ctd_1"
	ctd := NewCtd(name, nil)
	if ctd.Name() != name {
		t.Errorf("Wrong sensor name; expected %q, got %q", name, ctd.Name())
	}

	for _, e := range table {
		rec, err := ctd.ParseData(e.in)
		if err != nil {
			log.Fatal(err)
		}
		if !reflect.DeepEqual(rec, e.out) {
			t.Errorf("Parse failed; expected %#v, got %#v", e.out, rec)
		}
	}
}

func TestOptode(t *testing.T) {
	table := []struct {
		in  string
		out map[string]interface{}
	}{
		{
			in:  "4831\t317\t254.784\t25.287\t29.076\t29.076\t38.225\t9.149\t1081.4\t1073.2\t24.0",
			out: map[string]interface{}{"t": float32(25.287), "doconcs": float32(29.076)},
		},
	}

	const name = "optode_1"
	o := NewOptode(name, nil)
	if o.Name() != name {
		t.Errorf("Wrong sensor name; expected %q, got %q", name, o.Name())
	}

	for _, e := range table {
		rec, err := o.ParseData(e.in)
		if err != nil {
			log.Fatal(err)
		}
		if !reflect.DeepEqual(rec, e.out) {
			t.Errorf("Parse failed; expected %#v, got %#v", e.out, rec)
		}
	}
}

func TestNewOptode(t *testing.T) {
	table := []struct {
		in  string
		out map[string]interface{}
	}{
		{
			in:  "4831\t317\t254.784\t4.130\t25.287\t29.076\t29.076\t38.225\t9.149\t1081.4\t1073.2\t24.0",
			out: map[string]interface{}{"t": float32(25.287), "doconcs": float32(29.076)},
		},
	}

	const name = "optode_1"
	o := NewOptode(name, nil, Indicies([]int{4, 5}))
	if o.Name() != name {
		t.Errorf("Wrong sensor name; expected %q, got %q", name, o.Name())
	}

	for _, e := range table {
		rec, err := o.ParseData(e.in)
		if err != nil {
			log.Fatal(err)
		}
		if !reflect.DeepEqual(rec, e.out) {
			t.Errorf("Parse failed; expected %#v, got %#v", e.out, rec)
		}
	}
}

func TestAcm(t *testing.T) {
	table := []struct {
		in  string
		out map[string]interface{}
	}{
		{
			in: " -45.00,   11.66,  0.9359,  0.3361,  0.1057,  -46.36,    3.25,  -48.40,   -1.56",
			out: map[string]interface{}{
				"tx": float32(-45), "ty": float32(11.66),
				"hx": float32(0.9359), "hy": float32(0.3361), "hz": float32(0.1057),
				"va": float32(-46.36), "vb": float32(3.25), "vc": float32(-48.4), "vd": float32(-1.56),
			},
		},
	}

	const name = "acm_1"
	a := NewAcm(name, nil)
	if a.Name() != name {
		t.Errorf("Wrong sensor name; expected %q, got %q", name, a.Name())
	}

	for _, e := range table {
		rec, err := a.ParseData(e.in)
		if err != nil {
			log.Fatal(err)
		}
		if !reflect.DeepEqual(rec, e.out) {
			t.Errorf("Parse failed; expected %#v, got %#v", e.out, rec)
		}
	}
}

func TestFlntu(t *testing.T) {
	table := []struct {
		in  string
		out map[string]interface{}
	}{
		{
			in:  "99/99/99\t99:99:99\t695\t1275\t700\t4130\t533",
			out: map[string]interface{}{"chlaflo": int16(1275), "ntuflo": int16(4130)},
		},
	}

	const name = "flntu_1"
	f := NewFlntu(name, nil)
	if f.Name() != name {
		t.Errorf("Wrong sensor name; expected %q, got %q", name, f.Name())
	}

	for _, e := range table {
		rec, err := f.ParseData(e.in)
		if err != nil {
			log.Fatal(err)
		}
		if !reflect.DeepEqual(rec, e.out) {
			t.Errorf("Parse failed; expected %#v, got %#v", e.out, rec)
		}
	}

}

func TestFlcd(t *testing.T) {
	table := []struct {
		in  string
		out map[string]interface{}
	}{
		{
			in:  "99/99/99\t99:99:99\t460\t16380\t534",
			out: map[string]interface{}{"cdomflo": int16(16380)},
		},
	}

	const name = "flcd_1"
	f := NewFlcd(name, nil)
	if f.Name() != name {
		t.Errorf("Wrong sensor name; expected %q, got %q", name, f.Name())
	}

	for _, e := range table {
		rec, err := f.ParseData(e.in)
		if err != nil {
			log.Fatal(err)
		}
		if !reflect.DeepEqual(rec, e.out) {
			t.Errorf("Parse failed; expected %#v, got %#v", e.out, rec)
		}
	}

}
